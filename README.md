# Script PowerShell base et Avance

Scripting PowerShell Base et Avancé 

## Contenu du TP :

Création d'un script qui ferra :
* le déploiement des rôles suivants : AD, DNS, DHCP
* la création d'une forêt
* le paramétrage du DHCP


## Aller plus loin :
* Création d'une interface graphique dédiée pour le script en WinForm

## Screen de la GUI 
<p align="center">
<img src="/GUI/Screen/Screen_Gui.PNG" alt="Screen_Gui">
</p>

