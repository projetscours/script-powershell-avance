#requires -version 5
<#
.SYNOPSIS
  Script de deploiement des roles AD, DNS, DHCP 

.DESCRIPTION
  Script a envoyer sur le serveur (accompagne du fichier de fonctions) pour installer les roles AS, DNS, DHCP.
  Il permet l'installation des roles (AD, DNS, DHCP), la creation de la foret et le parametrage du DHCP en une commande.  

.PARAMETER Type
    Type d'installation : AD, DNS, DHCP, ALL (un seul choix possible et obligatoire).
.PARAMETER DomainNameDNS
    Si type d'installation AD ou ALL et qu'on veut creer une foret : nom FQDN de la foret
.PARAMETER DomainNameNetbios
    Si type d'installation AD ou ALL et qu'on veut creer une foret : nom NETBIOS de la foret
.PARAMETER DHCPDebutRange
    IP de debut de la range d'IP attribuees par le DHCP
.PARAMETER DHCPFinRange
    IP de fin de la range d'IP attribuees par le DHCP
.PARAMETER DHCPMasque
    Masque de sous reseau qui sera attribue par le DHCP
.PARAMETER DHCPScope
    Le scope IP sur lequel le DHCP va operer
.PARAMETER DHCPExcluDebutRange
    IP de debut de la range d'IP exclues par le DHCP
.PARAMETER DHCPExcluFinRange
    IP de fin de la range d'IP exclues par le DHCP
.PARAMETER DHCPDNSServer
    IP du serveur DNS
.PARAMETER DHCPDNSDomain
    Nom du domaine sur lequel le DHCP va operer (nom de la foret au format FQDN)
.PARAMETER DHCPRouter
    IP du routeur

.NOTES
  Version:        1.0
  Author:         Christopher LACROIX
  Creation Date:  24/04/2021
  Purpose/Change: Creation du script
  
.EXAMPLE
  .\setup.ps1 -type ALL
  .\setup.ps1 -type AD -DomainNameDNS "kimform.fr" -DomainNameNetbios "KIMFORM"
  .\setup.ps1 -type DHCP -DHCPDebutRange "192.168.1.220" -DHCPFinRange "192.168.1.230" -DHCPMasque "255.255.255.0" -DHCPScope "192.168.1.0" -DHCPExcluDebutRange "192.168.1.1" -DHCPExcluFinRange "192.168.1.229" -DHCPDNSServer "192.168.1.20" -DHCPDNSDomain "kimform.fr" -DHCPRouter "192.168.1.1"
#>


#---------------------------------------------------------[Parametres]--------------------------------------------------------

# Gestion des parametres 
param(
    [Parameter(Mandatory=$true)]
    [string]$Type,
    [Parameter(Mandatory=$false)]
    [string]$DomainNameDNS,
    [Parameter(Mandatory=$false)]
    [string]$DomainNameNetbios,
    [Parameter(Mandatory=$false)]
    [string]$DHCPDebutRange,
    [Parameter(Mandatory=$false)]
    [string]$DHCPFinRange,
    [Parameter(Mandatory=$false)]
    [string]$DHCPMasque,
    [Parameter(Mandatory=$false)]
    [string]$DHCPScope,
    [Parameter(Mandatory=$false)]
    [string]$DHCPExcluDebutRange,
    [Parameter(Mandatory=$false)]
    [string]$DHCPExcluFinRange,
    [Parameter(Mandatory=$false)]
    [string]$DHCPDNSServer,
    [Parameter(Mandatory=$false)]
    [string]$DHCPDNSDomain,
    [Parameter(Mandatory=$false)]
    [string]$DHCPRouter
)



#---------------------------------------------------------[Initialisations]--------------------------------------------------------

#Mettre les erreurs en Silently Continue (non bloquantes)
#$ErrorActionPreference = "SilentlyContinue"

#Chargement du fichier contenant les fonctions
. .\fonctions.ps1



#-----------------------------------------------------------[Execution]------------------------------------------------------------


########################################################
######## INSTALLATION ROLE(S)
########################################################
# Checks du parametre "Type"
write-host "####### INSTALLATION ROLE(S) "
write-host "####### Type d'installation : ${Type} "
# Condition suivant le type d'installation choisie
switch ( $Type )
{
    ALL
    {
        #Appel de la fonction d'installation des roles
        InstallRole -role AD-Domain-Services
        InstallRole -role DNS
        InstallRole -role DHCP
    }
    AD
    {
        #Appel de la fonction d'installation des roles
        InstallRole -role AD-Domain-Services
    }
    DNS
    {
        #Appel de la fonction d'installation des roles
        InstallRole -role DNS
    }
    DHCP
    {
        #Appel de la fonction d'installation des roles
        InstallRole -role DHCP
    }
    default
    {
        write-error "Veuillez saisir une valeur valide pour le parametre 'Type' !"
        exit 1
    }
}


########################################################
######## CREATION DE LA FORET 
########################################################
# Si DomainNameDNS et DomainNameNetbios remplis alors creation de foret
If (($DomainNameDNS) -and ($DomainNameNetbios))
{
  write-host "####### CREATION DE LA FORET AD "
  # Test si le type d'installation est bien ALL ou AD 
  if (($Type -eq "ALL") -or ($Type -eq "AD")) {
      #Appel de la fonction de creation de foret
      CreationForet -FQDN "${DomainNameDNS}" -NETBIOS "${DomainNameNetbios}"
  } else {
    write-error "Pour creer une foret, veuillez choisir un type d'installation AD ou ALL !"
    exit 1
  }
}


########################################################
######## PARAMETRAGE DU DHCP
########################################################
# Parametrage DHCP
If (($DHCPDebutRange) -and ($DHCPFinRange))
{
  write-host "####### Parametrage du DHCP "
  # Test si le type d'installation est bien ALL ou AD 
  if (($Type -eq "ALL") -or ($Type -eq "DHCP")) {
      #Avertissement quand au reboot avant parametrage du DHCP 
      write-warning "Avez vous redemarre le serveur apres la creation de la foret ? (o/n)"
      $AskReboot = Read-Host
      if ($AskReboot -eq "o") {
          write-host "L'operation peut continuer !"
      } else {
          write-error "Veuillez redemarrer le serveur avant de lancer le parametrage du DHCP (avec le Type 'DHCP') !"
          exit 1
      }

      #Appel de la fonction de creation de foret
      ParamDHCP -DHCPDebutRange "${DHCPDebutRange}" -DHCPFinRange "${DHCPFinRange}" -DHCPMasque "${DHCPMasque}" -DHCPScope "${DHCPScope}" -DHCPExcluDebutRange "${DHCPExcluDebutRange}" -DHCPExcluFinRange "${DHCPExcluFinRange}" -DHCPDNSServer "${DHCPDNSServer}" -DHCPDNSDomain "${DHCPDNSDomain}" -DHCPRouter "${DHCPRouter}" 
  } else {
    write-error "Pour parametrer le DHCP, veuillez choisir un type d'installation DHCP ou ALL !"
    exit 1
  }
}





# Info quand au reboot du serveur 
write-warning "Un reboot est conseille pour que la prise en charge des roles soit complete !"