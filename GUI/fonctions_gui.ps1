#############################################
####### Fonction d'installation de role
#############################################
Function InstallRole{
  param(
    [Parameter(Mandatory=$true)]
    [string]$role
  )  

  # Recuperation du statu du role 
  $StatuRole = ((Get-WindowsFeature -Name $role).InstallState)

  # Switch sur le statu du role 
  switch ( $StatuRole )
  {
      Installed
      {
          write-host "Le role ${role} est deja installe sur le serveur ..."
      }
      Available
      {
          write-host "Installation du role ${role} ..."
          Add-WindowsFeature -Name $role -IncludeManagementTools -IncludeAllSubFeature
          write-host "$role : Installation OK !" -ForegroundColor Green
      }
      default
      {
          write-error "Le role ${role} est dans un status inconnu !"
          exit 1
      }
  }
}


#############################################
####### Fonction de creation de la foret
#############################################
Function CreationForet{
  param(
    [Parameter(Mandatory=$true)]
    [string]$FQDN,
    [Parameter(Mandatory=$true)]
    [string]$NETBIOS
  )  

  write-host "Creation d'une foret qui a pour FQDN ${FQDN}, et pour nom NETBIOS ${NETBIOS} ..."

  # Configutation de la foret
  $ConfigurationForet = @{
  '-DatabasePath'= 'C:\Windows\NTDS';
  '-DomainMode' = 'Default';
  '-DomainName' = $FQDN;
  '-DomainNetbiosName' = $NETBIOS;
  '-ForestMode' = 'Default';
  '-InstallDns' = $true;
  '-LogPath' = 'C:\Windows\NTDS';
  '-NoRebootOnCompletion' = $true;
  '-SysvolPath' = 'C:\Windows\SYSVOL';
  '-Force' = $true;
  '-CreateDnsDelegation' = $false }

  # Importation du module ADDSDeployment
  Import-Module ADDSDeployment

  # Creation de la foret
  Install-ADDSForest @ConfigurationForet

  write-host "Installation de la foret OK " -ForegroundColor Green
}


#############################################
####### Fonction de parametrage du DHCP
#############################################
Function ParamDHCP{
  param(
    [Parameter(Mandatory=$true)]
    [string]$DHCPDebutRange,
    [Parameter(Mandatory=$true)]
    [string]$DHCPFinRange,
    [Parameter(Mandatory=$true)]
    [string]$DHCPMasque,
    [Parameter(Mandatory=$true)]
    [string]$DHCPScope,
    [Parameter(Mandatory=$false)]
    [string]$DHCPExcluDebutRange,
    [Parameter(Mandatory=$false)]
    [string]$DHCPExcluFinRange,
    [Parameter(Mandatory=$true)]
    [string]$DHCPDNSServer,
    [Parameter(Mandatory=$true)]
    [string]$DHCPDNSDomain,
    [Parameter(Mandatory=$true)]
    [string]$DHCPRouter
  )  

  #Serveur DHCP dans l'AD 
  write-host "Inscription du serveur DHCP dans l'AD ..."
  Add-DHCPServerInDC -DNSName $(hostname)
  if (! $?) { exit 1 }

  #Ajout du scope d'IP DHCP
  write-host "Ajout de la range d'IP disponibles ..."
  Add-DhcpServerv4Scope -Name "Range IP" -StartRange 192.168.1.220 -EndRange 192.168.1.230 -SubnetMask 255.255.255.0 -Description "Plage DHCP de base" -State Active
  if (! $?) { exit 1 }

  #Ajout du scope d'exclusion d'IP DHCP si renseigne
  if ($DHCPExcluDebutRange) {
    write-host "Ajout de la range d'IP exclues ..."
    Add-DhcpServerv4ExclusionRange -ScopeID 192.168.1.0 -StartRange 192.168.1.1 -EndRange 192.168.1.219
    if (! $?) { exit 1 }
  }
  

  #Authoriser le DHCP a operer sur le domaine 
  write-host "Authoriser le DHCP a operer sur le domaine ..."
  Set-DhcpServerv4OptionValue -ScopeID 192.168.1.0 -DNSServer 192.168.1.20 -DNSDomain kimform.fr -Router 192.168.1.1
  if (! $?) { exit 1 }


  #Dire au Gestionnaire de serveur que la config est faite
  write-host "Informer le gestionnaire de serveur que la configuration DHCP est faite ... "
  Set-ItemProperty –Path registry::HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\ServerManager\Roles\12 –Name ConfigurationState –Value 2


  write-host "Parametrage du DHCP OK " -ForegroundColor Green
}
