# Chargement des assemblies
[void][System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
[void][System.Reflection.Assembly]::LoadWithPartialName("System.Drawing")

#######################################
## FENETRE PRINCIPALE 
#######################################
# Creation de la form principale
$form = New-Object Windows.Forms.Form

# Pour bloquer le resize du form et supprimer les icones Minimize and Maximize
$form.FormBorderStyle = [System.Windows.Forms.FormBorderStyle]::FixedDialog
$form.MaximizeBox = $False
$form.MinimizeBox = $False

# Choix du titre
$form.Text = "Installation de rôles Windows Serveur"
# Choix de la taille
$form.Size = New-Object System.Drawing.Size(600,370)


#######################################
## TYPE D'INSTALLATION 
#######################################
# Groupement pour le type d'installation
$GroupBoxType = New-Object System.Windows.Forms.GroupBox
$GroupBoxType.Location = New-Object System.Drawing.Point(10,10)
$GroupBoxType.Width = 220
$GroupBoxType.Height = 100
$GroupBoxType.Text = "Rôles" 

# Label Type d'installation
$label_type = New-Object System.Windows.Forms.Label
$label_type.Location = New-Object System.Drawing.Point(20,40)
$label_type.Size = New-Object System.Drawing.Size(200,20)
$label_type.Text = "Choisissez un type d'installation : "

# Liste des types d'installation
$ComboBoxType = New-Object System.Windows.Forms.ComboBox
$ComboBoxType.Location = New-Object System.Drawing.Point(20,60)
$ComboBoxType.Size = New-Object System.Drawing.Size(200,10)
$ComboBoxType.Height = 80
[void] $ComboBoxType.Items.Add('ALL')
[void] $ComboBoxType.Items.Add('ADDS')
[void] $ComboBoxType.Items.Add('DNS')
[void] $ComboBoxType.Items.Add('DHCP')


#######################################
## PARAMETRES FORET 
#######################################
# Groupement pour les parametres de forêt
$GroupBoxForet = New-Object System.Windows.Forms.GroupBox
$GroupBoxForet.Location = New-Object System.Drawing.Point(10,130)
$GroupBoxForet.Width = 220
$GroupBoxForet.Height = 160
$GroupBoxForet.Text = "Forêt" 

# Label Nom de domaine DNS (FQDN)
$label_DomainNameDNS = New-Object System.Windows.Forms.Label
$label_DomainNameDNS.Location = New-Object System.Drawing.Point(20,160)
$label_DomainNameDNS.Size = New-Object System.Drawing.Size(200,20)
$label_DomainNameDNS.Text = "Nom de domaine FQDN : "

# TextBox Nom de domaine DNS (FQDN)
$TextBox_DomainNameDNS = New-Object System.Windows.Forms.TextBox
$TextBox_DomainNameDNS.Location = New-Object System.Drawing.Point(20,180)
$TextBox_DomainNameDNS.Size = New-Object System.Drawing.Size(200,20)

# Label Nom Netbios
$label_Netbios = New-Object System.Windows.Forms.Label
$label_Netbios.Location = New-Object System.Drawing.Point(20,230)
$label_Netbios.Size = New-Object System.Drawing.Size(200,20)
$label_Netbios.Text = "Nom NETBIOS : "

# TextBox Nom netbios
$TextBox_Netbios = New-Object System.Windows.Forms.TextBox
$TextBox_Netbios.Location = New-Object System.Drawing.Point(20,250)
$TextBox_Netbios.Size = New-Object System.Drawing.Size(200,20)



#######################################
## PARAMETRES DHCP
#######################################
# Groupement pour les parametres de forêt
$GroupBoxDHCP = New-Object System.Windows.Forms.GroupBox
$GroupBoxDHCP.Location = New-Object System.Drawing.Point(250,10)
$GroupBoxDHCP.Width = 330
$GroupBoxDHCP.Height = 320
$GroupBoxDHCP.Text = "DHCP" 

# Label Range DHCP
$label_DHCPRange = New-Object System.Windows.Forms.Label
$label_DHCPRange.Location = New-Object System.Drawing.Point(260,40)
$label_DHCPRange.Size = New-Object System.Drawing.Size(200,20)
$label_DHCPRange.Text = "Range d'attribution DHCP : "
# TextBox Debut range DHCP 
$TextBox_DHCPDebutRange = New-Object System.Windows.Forms.TextBox
$TextBox_DHCPDebutRange.Location = New-Object System.Drawing.Point(260,60)
$TextBox_DHCPDebutRange.Size = New-Object System.Drawing.Size(150,20)
$TextBox_DHCPDebutRange.Text = "IP de début"
# TextBox Fin range DHCP 
$TextBox_DHCPFinRange = New-Object System.Windows.Forms.TextBox
$TextBox_DHCPFinRange.Location = New-Object System.Drawing.Point(420,60)
$TextBox_DHCPFinRange.Size = New-Object System.Drawing.Size(150,20)
$TextBox_DHCPFinRange.Text = "IP de fin"

# Label Masque de sous-réseaux
$label_Masque = New-Object System.Windows.Forms.Label
$label_Masque.Location = New-Object System.Drawing.Point(260,100)
$label_Masque.Size = New-Object System.Drawing.Size(200,20)
$label_Masque.Text = "Masque de sous-réseaux : "
# TextBox Masque sous-réseaux 
$TextBox_Masque = New-Object System.Windows.Forms.TextBox
$TextBox_Masque.Location = New-Object System.Drawing.Point(260,120)
$TextBox_Masque.Size = New-Object System.Drawing.Size(150,20)

# Label Scope IP
$label_Scope = New-Object System.Windows.Forms.Label
$label_Scope.Location = New-Object System.Drawing.Point(420,100)
$label_Scope.Size = New-Object System.Drawing.Size(200,20)
$label_Scope.Text = "Scope IP : "
# TextBox Scope IP 
$TextBox_Scope = New-Object System.Windows.Forms.TextBox
$TextBox_Scope.Location = New-Object System.Drawing.Point(420,120)
$TextBox_Scope.Size = New-Object System.Drawing.Size(150,20)

# Label Range DHCP Exlus
$label_DHCPRangeExclu = New-Object System.Windows.Forms.Label
$label_DHCPRangeExclu.Location = New-Object System.Drawing.Point(260,160)
$label_DHCPRangeExclu.Size = New-Object System.Drawing.Size(200,20)
$label_DHCPRangeExclu.Text = "Range d'exclusion DHCP : "
# TextBox Debut range DHCP 
$TextBox_DHCPDebutRangeExclu = New-Object System.Windows.Forms.TextBox
$TextBox_DHCPDebutRangeExclu.Location = New-Object System.Drawing.Point(260,180)
$TextBox_DHCPDebutRangeExclu.Size = New-Object System.Drawing.Size(150,20)
$TextBox_DHCPDebutRangeExclu.Text = "IP de début"
# TextBox Fin range DHCP 
$TextBox_DHCPFinRangeExclu = New-Object System.Windows.Forms.TextBox
$TextBox_DHCPFinRangeExclu.Location = New-Object System.Drawing.Point(420,180)
$TextBox_DHCPFinRangeExclu.Size = New-Object System.Drawing.Size(150,20)
$TextBox_DHCPFinRangeExclu.Text = "IP de fin"

# Label IP serveur DNS
$label_IPDNS = New-Object System.Windows.Forms.Label
$label_IPDNS.Location = New-Object System.Drawing.Point(260,220)
$label_IPDNS.Size = New-Object System.Drawing.Size(150,20)
$label_IPDNS.Text = "IP du serveur DNS : "
# TextBox IP serveur DNS 
$TextBox_IPDNS = New-Object System.Windows.Forms.TextBox
$TextBox_IPDNS.Location = New-Object System.Drawing.Point(260,240)
$TextBox_IPDNS.Size = New-Object System.Drawing.Size(150,20)

# Label FQDN
$label_DHCP_FQDN = New-Object System.Windows.Forms.Label
$label_DHCP_FQDN.Location = New-Object System.Drawing.Point(420,220)
$label_DHCP_FQDN.Size = New-Object System.Drawing.Size(200,20)
$label_DHCP_FQDN.Text = "Nom de domaine FQDN : "
# TextBox Scope IP 
$TextBox_DHCP_FQDN = New-Object System.Windows.Forms.TextBox
$TextBox_DHCP_FQDN.Location = New-Object System.Drawing.Point(420,240)
$TextBox_DHCP_FQDN.Size = New-Object System.Drawing.Size(150,20)

# Label IP Routeur
$label_Routeur = New-Object System.Windows.Forms.Label
$label_Routeur.Location = New-Object System.Drawing.Point(260,280)
$label_Routeur.Size = New-Object System.Drawing.Size(200,20)
$label_Routeur.Text = "IP du routeur : "
# TextBox IP Routeur 
$TextBox_Routeur = New-Object System.Windows.Forms.TextBox
$TextBox_Routeur.Location = New-Object System.Drawing.Point(260,300)
$TextBox_Routeur.Size = New-Object System.Drawing.Size(150,20)



#######################################
## BOUTON
#######################################
# Bouton de validation 
$button_ok = New-Object System.Windows.Forms.Button
$button_ok.Text = "Lancer"
$button_ok.Size = New-Object System.Drawing.Size(220,30)
$button_ok.Location = New-Object System.Drawing.Size(10,300)



#######################################
## EVENTS 
#######################################
# Gestion event quand on clique sur le bouton OK
$button_ok.Add_Click(
{
    #Test sur le type d'installation choisie 
    switch ( $Type )
    {
        AD
        {
            #Appel de la commande avec seulement les parametres de foret
            .\script_gui.ps1 -type AD -DomainNameDNS $TextBox_DomainNameDNS.Text -DomainNameNetbios $TextBox_Netbios.Text
        }
        DHCP
        {
            #Appel de la commande avec seulement les paramétres DHCP
            .\script_gui.ps1 -type DHCP -DHCPDebutRange $TextBox_DHCPDebutRange.Text -DHCPFinRange $TextBox_DHCPFinRange.Text -DHCPMasque $TextBox_Masque.Text -DHCPScope $TextBox_Scope.Text -DHCPExcluDebutRange $TextBox_DHCPDebutRangeExclu.Text -DHCPExcluFinRange $TextBox_DHCPFinRangeExclu.Text -DHCPDNSServer $TextBox_IPDNS.Text -DHCPDNSDomain $label_DHCP_FQDN.Text -DHCPRouter $TextBox_Routeur.Text
        }
        DNS
        {
            #Appel de la commande simple DNS
            .\script_gui.ps1 -type DNS 
        }
        default
        {
            #Appel de la commande compléte 
            .\script_gui.ps1 -type ALL -DomainNameDNS $TextBox_DomainNameDNS.Text -DomainNameNetbios $TextBox_Netbios.Text -DHCPDebutRange $TextBox_DHCPDebutRange.Text -DHCPFinRange $TextBox_DHCPFinRange.Text -DHCPMasque $TextBox_Masque.Text -DHCPScope $TextBox_Scope.Text -DHCPExcluDebutRange $TextBox_DHCPDebutRangeExclu.Text -DHCPExcluFinRange $TextBox_DHCPFinRangeExclu.Text -DHCPDNSServer $TextBox_IPDNS.Text -DHCPDNSDomain $label_DHCP_FQDN.Text -DHCPRouter $TextBox_Routeur.Text
        }
    }
})



#######################################
## AFFICHAGE 
#######################################
# Ajout des différents éléments
# Forms Roles
$Form.controls.AddRange(@($label_type,$ComboBoxType))
# Forms Forêt
$Form.controls.AddRange(@($label_DomainNameDNS,$TextBox_DomainNameDNS,$label_Netbios,$TextBox_Netbios))
# Forms DHCP 1
$Form.controls.AddRange(@($label_Scope,$TextBox_Scope,$label_Masque,$TextBox_Masque,$TextBox_DHCPDebutRange,$TextBox_DHCPFinRange,$label_DHCPRange))
# Forms DHCP 2
$Form.controls.AddRange(@($TextBox_DHCPFinRangeExclu,$TextBox_DHCPDebutRangeExclu,$label_DHCPRangeExclu))
# Forms DHCP 3
$Form.controls.AddRange(@($label_IPDNS,$TextBox_IPDNS,$label_DHCP_FQDN,$TextBox_DHCP_FQDN,$label_Routeur,$TextBox_Routeur))

# Forms groupBox
$Form.controls.AddRange(@($GroupBoxDHCP,$GroupBoxType,$GroupBoxForet,$button_ok))

# Affichage de la Windows
$form.ShowDialog()


